package com.springboot.demo.hellocontroller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class hellocontroller {
    @RequestMapping("/helloworld")
    public String helloworld() {
        return "Hello World!";
    }
}
